# Projeto criado para executar os Testes Uniários, SonarQube e QualityGate em uma Pipeline do Jenkins

* App e criação dos testes baseado no curso da Alura:
https://cursos.alura.com.br/course/nodejs-testes-unitarios-integracao/

* Instalado docker

* Baixado a imagem do Jenkin e do Sonarqube

* Criado uma pipeline no Jenkins usando o Jenkinfile da raiz do projeto

* Criado um projeto no Sonarqube

* Após a criação do projeto, copiei os dados para serem usados no arquivo sonar-project.properties na raiz do projeto

* Criado um webhook no Sonarqube com o endereço do jenkins - http://localhost:8080/sonarqube/

* Instalado o plugin do Sonarqube no  jenkins e configurado o SonarQube servers com a url do Sonar

